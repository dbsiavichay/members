import { Module } from '@nestjs/common';
import { MemberModule } from './member/member.module';
import { AppController } from './app.controller';
import { PrismaModule } from './prisma/prisma.module';

@Module({
  imports: [MemberModule, PrismaModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
