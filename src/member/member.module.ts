import { Module } from '@nestjs/common';
import { MemberService } from './member.service';
import { MemberListController, MemberCreateController } from './member.controller';

@Module({
  providers: [MemberService],
  controllers: [MemberListController, MemberCreateController]
})
export class MemberModule {}
