import { Controller, Get, Render } from '@nestjs/common';

@Controller('members')
export class MemberListController {
    @Get()
    @Render('member/list')
    get() {
        const title = 'Listado de miembros'
        const members = []
        return {title, members}
    }
}

@Controller('members')
export class MemberCreateController {
    @Get('create')
    @Render('member/form')
    get() {
        const title = 'Crear nuevo miembro'
        const members = []
        return {title, members}
    }
}