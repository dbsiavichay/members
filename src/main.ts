import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { create } from 'express-handlebars';
import { join } from 'path';

const hbs = create({ 
  extname: 'hbs',
  layoutsDir: 'views/base',
  partialsDir: 'views/base/partials',
  defaultLayout: 'base'
})

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.useStaticAssets(join(__dirname, '..', 'public'))
  app.engine('hbs', hbs.engine);
  app.setViewEngine('hbs');
  await app.listen(3000);
}
bootstrap();
