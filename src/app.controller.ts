import { Get, Controller, Render } from '@nestjs/common';

@Controller()
export class AppController {
  @Get()
  @Render('index')
  render() {
    const title = 'Título'
    const message = 'Hola mundo!';
    return { title, message };
  }
}